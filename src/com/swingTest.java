package com;

import com.eltima.components.ui.DatePicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.Locale;

public class swingTest {

    public static void main(String[] args) {
        //初始化窗口
        JFrame jFrame = new JFrame("T");
        //关闭窗口的事件
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //设置宽度，高度
        jFrame.setSize(300,150);

        //初始化面板
        JPanel jPanel = new JPanel();
        jFrame.add(jPanel);
        jPanelDefault(jPanel);

        //设置窗口界面可见
        jFrame.setVisible(true);

    }

    //输入框
    static  JTextField JTextField0 = new JTextField();
    //时间选择框
    static DatePicker datepick = getDatePicker();

    public static void jPanelDefault(JPanel jPanel){
        //面板布局设置为空，就可以手动设置组件坐标和大小了
        jPanel.setLayout(null);

        //标签
        JLabel jLabel0 = new JLabel("备注：");
        jLabel0.setBounds(20, 10, 80, 20);
        jPanel.add(jLabel0);

        //输入框
        JTextField0.setColumns(20);
        JTextField0.setBounds(120, 10, 150,20);
        jPanel.add(JTextField0);

        //标签
        JLabel jLabel1 = new JLabel("时间：");
        jLabel1.setBounds(20, 40, 80, 20);
        jPanel.add(jLabel1);

        //时间选择框
        datepick.setBounds(120, 40, 150, 20);
        jPanel.add(datepick);

        //按钮
        JButton jButton0 = new JButton("确认");
        jButton0.setBounds(60, 80, 60, 20);
        //按钮事件
        jButton0.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "保存成功"+JTextField0.getText()+"; 时间："+datepick.getText());
            }
        });
        jPanel.add(jButton0);

        //按钮
        JButton jButton1 = new JButton("取消");
        jButton1.setBounds(160, 80, 60, 20);
        //按钮事件
        jButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //关闭弹窗
                System.exit(0);
            }
        });
        jPanel.add(jButton1);

    }


    private static DatePicker getDatePicker() {
        final DatePicker datepick;
        // 格式
        String DefaultFormat = "yyyy-MM-dd HH:mm:ss";
        // 当前时间
        Date date = new Date();
        // 字体
        Font font = new Font("Times New Roman", Font.BOLD, 14);

        Dimension dimension = new Dimension(177, 24);

        int[] hilightDays = { 1, 3, 5, 7 };

        int[] disabledDays = { 4, 6, 5, 9 };
        //构造方法（初始时间，时间显示格式，字体，控件大小）
        datepick = new DatePicker(date, DefaultFormat, font, dimension);

        datepick.setLocation(137, 83);//设置起始位置
        /*
        //也可用setBounds()直接设置大小与位置
        datepick.setBounds(137, 83, 177, 24);
        */
        // 设置一个月份中需要高亮显示的日子
        datepick.setHightlightdays(hilightDays, Color.red);
        // 设置一个月份中不需要的日子，呈灰色显示
        datepick.setDisableddays(disabledDays);
        // 设置国家
        datepick.setLocale(Locale.CANADA);
        // 设置时钟面板可见
        datepick.setTimePanleVisible(true);
        return datepick;
    }

}
